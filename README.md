# qutebrowser-metascript

Metascript is a user configurable arbitrary sequential command running userscript for qutebrowser, written in python.

It can be used to automate a sequence of qutebrowser commands, userscripts and user inputs (see Limitations on this) arbitrarily.

You can pass arguments to it to be passed to other commands or userscripts.
With this, any set of installed userscripts or qutebrowser commands can be chained to a keybind or configured command.

Each metascript is saved as a file with nothing more than a list of commands, and called as a command to the metascript userscript. 

It's basically like shell scripting for the qutebrowser command input.

## Why?

Suppose you want to for example clear finished downloads, and then run a userscript that clears all cookies, every time you close a tab?
simply bind Metascript and a command (metascript) file to the key(s) (in the above example, "d"), with the command/metascript file containing all the commands you want to run!

## Limitations

Qutebrowser can have race conditions when running commands from userscripts. Because of this, a hard wait is implemented at the end of each command run of 0.3 seconds. You might still have trouble from time to time and if so, open script-runner.py and adjust wait_time to your needs.

Qutebrowser commands are run in order. There is no decision tree functionality or anything like that. You can only run preconfigured, sequential operations with this userscript.

Metascripts can only be run one at a time. These levels of irony shouldn't even be possible!

```set-cmd-text``` can only be used as the last command to run. To get user input otherwise, pass arguments directly to metascript instead and specify which arguments go to which commands in your script.

Honestly it makes sense to never use ```set-cmd-text```, and always pass arguments to metascript to be passed to the qutebrowser commands your metascript runs.

I would love to be able to pass outputs of some userscripts as inputs to others, but I do not believe it is possible in qutebrowser. If I am wrong about this let me know!

I wanted to be able to run commands in the shell to return values to other scripts as well, but this complicates things a lot and doesn't seem too useful. If I can come up with a good reason to do it I might hack on it some time.

## Usage

To run:
```
:spawn --userscript path/to/script-runner.py /abspath/to/command/files/dir/ <command> <?arg> <?arg>
````
The command corresponds to the file name of a metascript in the command files directory. this directory is where you place the metascripts you want to call.

a metascript looks like this:
```
# comment
:command # comment
:other_command |2| # the number between pipes corresponds to the index, iterating from 1, of the argument passed to metascript that you want to pass to this command
```
That's it, that's the format, it's just comments, commands and integers between pipes.

As an example, suppose you create a metascript called "open-and-tab-only" that looks like this:
```
# open a link and then close all other tabs
:open -t |1|
:tab-only
```
And suppose you want to run it and open "weboasis.app".  You would run it like this:
```
:spawn --userscript path/to/script-runner.py /abspath/to/command/files/dir/ open-and-tab-only weboasis.app
````
Qutebrowser will open weboasis.app and then close all other tabs.

You can call other userscripts from within a metascript just the same as you would a command in qutebrowser.

## Dependencies

Metascript depends on os, sys and time from the python standard library.

## Contributing

This userscript is considered to be feature complete. If you find bad behavior or think that there is some important functionality congruent with the scope of the project, feel free to open an issue or a PR.

Qutebrowser is a very niche browser and as such, tools that extend the browser in powerful ways are hard to come by. If you like this (or anything else I have done) and feel like buying me a beer feel free to send me a few bucks:

XMR (preferred): 46zFAv8KHaKVuYDTJ15TXAah6SCXw88Dx9UhTuUJa6ydb8m9uGLaYE3AX5JPFhsJjJ6w7NMc7vNYwQPhGkt3tE2L7pwgrte

BTC: bc1qmulvfg7ctj0crpfvq9wux5j8xg9qskrj6xcf0x

LTC (in case BTC is too expensive): ltc1qrh6l8rf0pr0upalcjr2de4hc8xzczcg7slmygv

ETH: 0xAF5621F1f484aa10042Ab84c4C5D436C89D06262

I will give some portion (at my discretion) of any donations I get to The-Compiler for maintaining Qutebrowser, as well as to other FOSS tools I use.

You can always do that as well! Many of us put a lot of effort into things just because we want to see them exist and make people's lives better, and it is always nice when people appreciate and support us for what we do.
