#!/usr/bin/python3

from os import environ as environ
from time import sleep as sleep
from sys import argv as argv

# run a qutebrowser command
def run_qb_command(command):
    with open(environ["QUTE_FIFO"], "a") as output:
        output.write(command)
    sleep(wait_time)

# open and read the command script content
def read_metascript(scripts_path, script):
    with open(scripts_path + script, "r") as script_file:
        script_content = script_file.read()
    return script_content

# parse script content to get only the commands
def parse_metascript(script_content):
    commands_list = []
    for line in script_content.split("\n"):
        if line == "":
            pass
        elif line[0] == "#":
            pass
        elif line[0] == ":":
            command = line.split("#")[0]
            commands_list.append(command)
        else:
            pass
    return commands_list

# replace all variables in a command with corresponding arguments
def replace_args(args, command):
    command = command.split("|")
    for part in command:
        try:
            arg_index = int(part) - 1
            command[command.index(part)] = args[arg_index]
        except:
            pass
    command = "".join(command)
    return command

def run():
    scripts_path = argv[1]
    script = argv[2]
    args = argv[3:]
    commands = parse_metascript(read_metascript(scripts_path, script))
    for command in commands:
        command = replace_args(args, command)
        run_qb_command(command)

wait_time = 0.3
run()